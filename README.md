# opensuse-leap

# server mini install

image-opensuse-leap-15.3-amd64-x64-server-mini-sda-sda1-1050623-efi-bootable-rel1664438403.img.gz

image-opensuse-leap-15.3-amd64-x64-server-mini-sda-sda1-1050623-efi-bootable-rel1664438403.img.gz.md5 

3dcff49b12417c11942a1b53d23f620a  image-opensuse-leap-15.3-amd64-x64-server-mini-sda-sda1-1050623-efi-bootable-rel1664438403.img

````
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 7A32F767-AF36-489D-B4C2-324FA39ADB22
Device       Start      End  Sectors  Size Type
/dev/sdb1     2048  1050623  1048576  512M EFI System
/dev/sdb2  1050624 15646686 14596063    7G Linux filesystem
````


````
localhost:/home/user # dd if=/dev/sdb  count=15646688 | gzip -9  >  image-opensuse-leap-15.3-amd64-x64-server-mini-sda-sda1-1050623-efi-bootable-rel1664442325 
15646688+0 records in
15646688+0 records out
8011104256 bytes (8.0 GB, 7.5 GiB) copied, 61.0781 s, 131 MB/s

localhost:/home/user # cat image-opensuse-leap-15.3-amd64-x64-server-mini-sda-sda1-1050623-efi-bootable-rel1664442325-sdb2free.img.gz.md5 
031b1a2779c3b2a7aca5396d3f35f699  image-opensuse-leap-15.3-amd64-x64-server-mini-sda-sda1-1050623-efi-bootable-rel1664442325-sdb2free.img.gz
````


````
c:\apps\qemu>qemu-system-x86_64.exe -boot c -drive file=c:\apps\flde-live-memsti
ck-image-1655222827-net-v1.5.2-152-1-x86_64_amd64-1vd.img,index=0,media=disk,for
mat=raw -drive file=opensuse-leap.vmdk,index=1,media=disk,format=vmdk
````



